package numbers;
import java.util.ArrayList;


public class Numbers {
//	static int[] numbers = new int[]{234, 678, 131, 101, 199, 346};
	static int[] numbers = new int[]{4,2,1,3, 10};


	public static void main(String[] args)
	{
		
		ArrayList <Integer> palindromes = isPalindrome();
		ArrayList <Integer> primes= primeNumbers();
		
		  System.out.println(" Largest Number: "+ maxValue());
		  System.out.println(" Smallest Number: "+ minValue());
		  for(int i=0; i<palindromes.size();i++)
			  System.out.println(" Palindrome: "+ palindromes.get(i) );
		  for(int i=0; i<primes.size();i++)
			  System.out.println(" Prime Number: "+ primes.get(i));
		   
		 
		  
		
		}
          	
public static int maxValue(){
	
	    int largest = numbers[0];
	    
    for(int i=1; i< numbers.length; i++)
    {
            if(numbers[i] > largest)
                    largest = numbers[i];
            
           
    }
   
    return largest;
    
}

public static int minValue(){
	int smallest = numbers[0];
    
    
    for(int i=1; i< numbers.length; i++)
    {
            if (numbers[i] < smallest)
                    smallest = numbers[i];
           
    }
    return smallest;
	
}

public static ArrayList<Integer> isPalindrome(){
	ArrayList<Integer> palindromes = new ArrayList<Integer>();
	for (int i=0; i<numbers.length;i++)
	{
		int palindrome = numbers[i];
		int reversed =0;
		int temp=0;
		while(palindrome!=0)
		{
			 temp = palindrome % 10;
             palindrome = palindrome / 10;
             reversed = reversed * 10 + temp;
		}
		if(numbers[i]==reversed)
			palindromes.add(numbers[i]);
		
	}
	return palindromes;

}

public static ArrayList<Integer> primeNumbers()
{
	ArrayList<Integer> primes=new ArrayList<Integer>();
	for(int i=0;i<numbers.length;i++){
	int temp=0;
	boolean isPrime=true;
	int num=numbers[i];
	for (int j=2;j<=num/2;j++){
		{
	       temp=num%j;
		   if(temp==0)
		   {
		      isPrime=false;
		      break;
		   }
		}
	}
	if(isPrime)
		   primes.add(numbers[i]);
	}
	return primes;


}
}