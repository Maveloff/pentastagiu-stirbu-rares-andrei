package geometricfigures;
import java.util.Scanner;

 public class Shapes {

	private static Scanner sc;
	
	public static void main(String args[])
	{
		int figures=0;
		
		 sc=new Scanner(System.in);
		
		System.out.println("\n This program calculates the Area and Perimeter for Triangle, Circle, Rectangle \n");
		System.out.println("\n Insert 1 for Triangle \n");
		System.out.println("\n Insert 2 for Circle \n");
		System.out.println("\n Insert 3 for Rectangle \n");
		
		figures=sc.nextInt();
		
		if (figures==1)
		{
			Triangle triangle = new Triangle();
			System.out.println(" Area: " +triangle.area()+ " Perimeter: "+triangle.perimeter() );
			return ;
		}
		if(figures==2)
		{
			Circle circle = new Circle();
			System.out.println(" Area: " +circle.area()+ " Circumference: "+circle.circumference() );
			return;
		}
		if(figures==3)
		{
			Rectangle rectangle=new Rectangle();
			System.out.println(" Area: "+rectangle.area()+ " Perimeter: "+rectangle.perimeter());
			return;
		}
		
		
			System.out.println(" Insert the right numbers (1,2,3)");
			main(args);
		
			}
}

	
	
