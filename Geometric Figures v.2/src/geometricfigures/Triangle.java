package geometricfigures;
import java.util.Scanner;

public class Triangle extends Shapes{
	
		private double side1, side2, side3;
	
		private Scanner sc;
		
		public Triangle(){
			getsides();
		}
	 
		public double area(){
			
			
			double peri=perimeter()/2;
			double area = Math.sqrt(peri*(peri-side1)*(peri-side2)*(peri-side3));
			
			return area;
			
		}
		
		public double perimeter(){
			
			double perimeter = side1 + side2 + side3;
			
			return perimeter;
			
		}
		
		public void getsides(){
		sc = new Scanner(System.in);
		
		System.out.println("\n Sides value: ? ");
		side1 = sc.nextDouble();
		side2 = sc.nextDouble();
		side3 = sc.nextDouble();				   
	
		}
}
