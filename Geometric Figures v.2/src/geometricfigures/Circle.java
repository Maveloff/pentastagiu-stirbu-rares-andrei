package geometricfigures;
import java.util.Scanner;

public class Circle extends Shapes{
	
	double diameter, pi, r, circumference, area;
	
	private static Scanner sc;
	
	public Circle(){
		getDiameter();
	}
	
	public void getDiameter(){
		sc = new Scanner(System.in);
		
		System.out.println("\n Diameter: ? \n");
		diameter=sc.nextDouble();
		pi=3.14159265359;
		
	}
		public double circumference(){
			
		r=diameter/2;
		circumference= 2*pi*r;
		return circumference;
		
	}
		
		public double area(){
			
			r=diameter/2;	
		area=r*r*pi;
		return area;
		
	}
		
}
